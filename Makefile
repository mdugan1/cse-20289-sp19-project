CC=		gcc
CFLAGS=		-g -Wall -Werror -std=gnu99 -Iinclude
LD=		gcc
LDFLAGS=	-L.
AR=		ar
ARFLAGS=	rcs
TARGETS=	bin/spidey

all:		$(TARGETS)

clean:
	@echo Cleaning...
	@rm -f $(TARGETS) lib/*.a src/*.o *.log *.input

.PHONY:		all test clean

# TODO: Add rules for bin/spidey, lib/libspidey.a, and any intermediate objects

run:    bin/spidey
	@./bin/spidey

src/forking.o:	src/forking.c
	@echo COMPILING src/forking.o...
	$(CC) $(CFLAGS) -c -o $@ $^

src/handler.o:	src/handler.c
	@echo COMPILING src/handler.o...
	$(CC) $(CFLAGS) -c -o $@ $^

src/request.o: 	src/request.c
	@echo COMPILING src/request.o...
	$(CC) $(CFLAGS) -c -o $@ $^

src/single.o:	src/single.c
	@echo COMPILING src/single.o...
	$(CC) $(CFLAGS) -c -o $@ $^

src/socket.o: 	src/socket.c
	@echo COMPILING src/socket.o...
	$(CC) $(CFLAGS) -c -o $@ $^

src/utils.o: 	src/utils.c
	@echo COMPILING src/utils.o...
	$(CC) $(CFLAGS) -c -o $@ $^

src/spidey.o: 	src/spidey.c
	@echo COMPILING src/spidey.o...
	$(CC) $(CFLAGS) -c -o $@ $^

lib/libspidey.a: 	src/forking.o src/utils.o src/single.o src/socket.o src/request.o src/handler.o
	@echo LINKING lib/libspidey.a...
	@$(AR) $(ARFLAGS) $@ $^

bin/spidey: 	src/spidey.o lib/libspidey.a
	@echo LINKING bin/spidey...
	@$(LD) $(LDFLAGS) -o $@ $^

