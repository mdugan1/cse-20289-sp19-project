#!/usr/bin/env python3

import multiprocessing
import os
import requests
import sys
import time

# Globals

PROCESSES = 1
REQUESTS  = 1
VERBOSE   = False
URL       = None

# Functions

def usage(status=0):
    print('''Usage: {} [-p PROCESSES -r REQUESTS -v] URL
    -h              Display help message
    -v              Display verbose output

    -p  PROCESSES   Number of processes to utilize (1)
    -r  REQUESTS    Number of requests per process (1)
    '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def do_request(pid):
    ''' Perform REQUESTS HTTP requests and return the average elapsed time. '''
    totalTime = 0
    for i in range(REQUESTS):
        t0 = time.time()
        response = requests.get(URL) #actually get the shtuff
        tf = time.time()
    
        if VERBOSE: #print the text of the response if verbose
            print(response.text)

        deltaT = tf-t0
        totalTime = totalTime +(deltaT) # update the total time
        print("Process: {}, Request: {}, Elapsed Time: {:.2f}".format(pid,i,deltaT))
    
    avgTime = totalTime / (REQUESTS)
    print("Process: {}, AVERAGE Elapsed Time: {:.2f}".format(pid,avgTime))

    return avgTime

# Main execution

if __name__ == '__main__':
    # Parse command line arguments
    args = sys.argv[1:] # get argmuents from command line

    while len(args) and args[0].startswith('-') and len(args[0]) >=2: #get all '-' options; break out when the list is empty, too short, or is a URL
        arg = args.pop(0);
        if arg == "-h":
            usage(0)
        elif arg == "-v":
            VERBOSE = True
        elif arg == "-r":
            REQUESTS = int(args.pop(0))
        elif arg == "-p":
            PROCESSES = int(args.pop(0))
        else: #invaid
            usage(1)

    if len(args) == 1: # if there is a url, set URL to that
        URL = args[0]
    else: #there is no URL or there is a space somewhere so it died
        usage(1)
    
    # Create pool of workers and perform requests
    allTheCores = multiprocessing.Pool(PROCESSES)
    allTheResults = allTheCores.map(do_request,range(PROCESSES))

    avgTime = 0
    for result in allTheResults:
        avgTime = avgTime + result
    avgTime = avgTime / PROCESSES


    print("TOTAL Average Elapsed Time: {:.2f}".format(avgTime))
    
    pass 

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
