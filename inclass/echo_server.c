#include<errno.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>


#include<netdb.h>
#include<netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

const char *HOST = NULL;
const char *PORT = "9420";

int socket_listen(const char* host, char* port);

int main(int argc,char *argv[]){
	//lookup server address info
	struct addrinfo *results;
	struct addrinfo hints = {
		.ai_family = AF_UNSPEC,
		.ai_socktype = SOCK_STREAM,
		.ai_flags = AI_PASSIVE, //listen on all interfaces
	};
	
	int status = getaddrinfo(HOST,PORT,&hints,&results);
	if(status!=0){
		return EXIT_FAILURE;
	}

	//for each server entry, allocate socket and try to connect
	
	int server_fd = -1l
	for(struct addrinfo *p = results; p && server_fd < 0; p = p->ai_next){
		server_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if(server_fd<1){
			continue;
		}
		
		//bind socket
		if(bind(server_fd, p-<ai_addr, p->aiaddrlen)<0){
			close(server_fd);
			server_fd = 1;
			continue;
		}

		//listen socket
		if(listen(server_fd, SOMAXCONN)<0){
			close(server_fd);
			server_fd = -1;
		}

		//release allocated address information
		freeaddrinfo(results);

		if(server_fd<0){
			return EXIT_FAILURE;
		}
	}
	
	//process incoming connections
	while(true){
		//accept incoming connection
		int client_fd = -1;
		struct sockaddr client_addr;
		socklen_t	client_len = sizeof(struct sockaddr);
		
		if(cliend_fd<0){
			continue;
		}
	
		pid_t p = fork();
		//open file stream from socket file descriptor
		FILE *client_stream = fdopen(client_fd, "r+");
		if(!client_stream){
			close(clientfd);
			continue;
		}



		pid_t p = fork();
                if(pid<0){ //error
                        fclose(client_stream);
                        continue;
                } else if(pid == 0){ //child
			//read from client and then echo back
		        char buffer[BUFSIZ];
			while(fgets(buffer,BUFSIZ,client_stream)){
				fputs(buffer,stdout);
				fputs(buffer,client_stream);
			}
			exit(EXIT_SUCCESS); //exit successfully from the child
                } else { //parent

                }



/*		//read from client and then echo back
		char buffer[BUFSIZ];
		while(fgets(buffer,BUFSIZ,client_stream)){
			fputs(buffer,stdout);
			fputs(buffer,client_stream);
		}
*/		
		//close connection
		fclose(client_stream);
	}
	
	return EXIT_SUCCESS;
	
}
