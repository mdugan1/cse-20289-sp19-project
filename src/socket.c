/* socket.c: Simple Socket Functions */
//Code by Michael Dugan
//Inspired by code given to the class from Dr. Peter Bui

#include "spidey.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

/**
 * Allocate socket, bind it, and listen to specified port.
 *
 * @param   port        Port number to bind to and listen on.
 * @return  Allocated server socket file descriptor.
 **/
int socket_listen(const char *port) {
    /* Lookup server address information */
    int status;
    struct addrinfo *results;
    struct addrinfo hints = { //specifies type of file
        .ai_socktype = SOCK_STREAM,
        .ai_family = AF_UNSPEC,
        .ai_flags = AI_PASSIVE
    };
    
    if((status = getaddrinfo(NULL,port,&hints,&results))){
        printf("ERROR - UNABLE TO USE getaddrinfo: %s \n", gai_strerror(status));
        return EXIT_FAILURE; 
    }
    
    /* For each server entry, allocate socket and try to connect */
    int socket_fd = -1;
    for (struct addrinfo *p = results; p != NULL && socket_fd < 0; p = p->ai_next) {
	/* Allocate socket */
        if((socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
    printf("ERROR - UNABLE TO MAKE A SOCKET: %s \n", strerror(errno)); //print error if no socket, otherwise I just made a socket with a file descriptor
}

	/* Bind socket */
        if(bind(socket_fd, p->ai_addr, p-> ai_addrlen) == -1){ //if error in binding, close and reset socket and try again
            printf("ERROR - UNABLE TO BIND TO CLIENT: %s \n", strerror(errno));
            close(socket_fd);
            socket_fd = -1;
            continue;
        }


    	/* Listen to socket */
        if(listen(socket_fd, SOMAXCONN) == -1){ //on failure, close socket_fd and continue to next iteration, so that we can try again
            printf("ERROR - LISTENING FAILED; GO BACK TO KINDERGARTEN %s \n",strerror(errno));
            close(socket_fd);
            socket_fd = -1;
            continue;
        }
    }

    freeaddrinfo(results);
    return socket_fd;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
