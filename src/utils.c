/* utils.c: spidey utilities */

#include "spidey.h"

#include <ctype.h>
#include <errno.h>
#include <string.h>

#include <sys/stat.h>
#include <unistd.h>

/**
 * Determine mime-type from file extension.
 *
 * @param   path        Path to file.
 * @return  An allocated string containing the mime-type of the specified file.
 *
 * This function first finds the file's extension and then scans the contents
 * of the MimeTypesPath file to determine which mimetype the file has.
 *
 * The MimeTypesPath file (typically /etc/mime.types) consists of rules in the
 * following format:
 *
 *  <MIMETYPE>      <EXT1> <EXT2> ...
 *
 * This function simply checks the file extension version each extension for
 * each mimetype and returns the mimetype on the first match.
 *
 * If no extension exists or no matching mimetype is found, then return
 * DefaultMimeType.
 *
 * This function returns an allocated string that must be free'd.
 **/
char * determine_mimetype(const char *path) {
    char *ext;
    char *mimetype;
    char *token;
    char buffer[BUFSIZ];
    FILE *fs = NULL;

    /* Find file extension */
    
    //use strrchr to return the substring after and including the last period
    ext = strrchr(path, '.');    
    if(ext == NULL){
        goto dieGracefully;
    }


    ext++;
    /* Open MimeTypesPath file */
    fs = fopen(MimeTypesPath,"r");
    if(fs == NULL){
        log("INCORRECT FOPEN DETECTED; %s",strerror(errno));
        goto dieGracefully;
    }


    /* Scan file for matching file extensions */
    while(fgets(buffer,BUFSIZ,fs)){
        mimetype = strtok(skip_whitespace(buffer),WHITESPACE);
        if(mimetype==NULL){
            continue; //keepy trying if no valid mimetype until you run out of attempts
        }

        while((token = strtok(NULL,WHITESPACE))){
            if(streq(token,ext)){
                if(fs){
                    fclose(fs);
                    goto howToSucceed;
                }
            }

           // token = strtok(NULL,WHITESPACE);
        }
    }    


dieGracefully:
    //copy over mimetype from default to now.
    mimetype = calloc(sizeof(char),strlen(DefaultMimeType)+1);
    strcpy(mimetype,DefaultMimeType);
    return mimetype;

howToSucceed:
    return strdup(mimetype);
}

/**
 * Determine actual filesystem path based on RootPath and URI.
 *
 * @param   uri         Resource path of URI.
 * @return  An allocated string containing the full path of the resource on the
 * local filesystem.
 *
 * This function uses realpath(3) to generate the realpath of the
 * file requested in the URI.
 *
 * As a security check, if the real path does not begin with the RootPath, then
 * return NULL.
 *
 * Otherwise, return a newly allocated string containing the real path.  This
 * string must later be free'd.
 **/
char * determine_request_path(const char *uri) {
    char temp[BUFSIZ];
    char path[BUFSIZ];

    if(uri == NULL || uri[0] == 0x0){
        return NULL;
    }
    
    int i = snprintf(temp,BUFSIZ,"%s/%s",RootPath,uri);
    if(i<0){ //negative values indicate output issues
        return NULL;
    }

    realpath(temp,path); //get the real path into path

    if(strncmp(path,RootPath,strlen(RootPath))){ //rootpath should always be smaller or equal to, because it does not include the uri while the path does
        return NULL;
    }

    //allocate to the stack and return the proper values
    char * returnable = (char *) calloc(sizeof(char),strlen(path)+1);
    strcpy(returnable,path);
    return returnable;
}

/**
 * Return static string corresponding to HTTP Status code.
 *
 * @param   status      HTTP Status.
 * @return  Corresponding HTTP Status string (or NULL if not present).
 *
 * http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 **/
const char * http_status_string(Status status) {
    static char *StatusStrings[] = {
        "200 OK",
        "400 Bad Request",
        "404 Not Found",
        "500 Internal Server Error",
        "418 I'm A Teapot",
    };
    char* status_string = StatusStrings[4];
    if(status == HTTP_STATUS_OK){
        status_string = StatusStrings[0];
    } else if(status == HTTP_STATUS_BAD_REQUEST){
        status_string = StatusStrings[1];
    } else if(status == HTTP_STATUS_NOT_FOUND){
        status_string = StatusStrings[2];
    } else if(status == HTTP_STATUS_INTERNAL_SERVER_ERROR){
        status_string = StatusStrings[3];
    }/* else if(status == HTTP_STATUS_IM_A_TEAPOT){
        status_string = StatusStrings[3]; //I don't know why it is saying this is broken but it does
    } */else {
        status_string = StatusStrings[1];
    }

    return status_string;
}

/**
 * Advance string pointer pass all nonwhitespace characters
 *
 * @param   s           String.
 * @return  Point to first whitespace character in s.
 **/
char * skip_nonwhitespace(char *s) {
    while(!isspace(*s) && *s){ //so long as s is not a space and s exists
        s++; //keep iterating till you find one
    }

    return s; //note that if this returns null, it whould be taken that there is no whitespace in the field.
}

/**
 * Advance string pointer pass all whitespace characters
 *
 * @param   s           String.
 * @return  Point to first non-whitespace character in s.
 **/
char * skip_whitespace(char *s) {
    while(isspace(*s) && *s){ //same logic as above, but flipped for skipping whitespace instead
        s++;
    }

    return s;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
