/* forking.c: Forking HTTP Server */

#include "spidey.h"

#include <errno.h>
#include <signal.h>
#include <string.h>

#include <unistd.h>

/**
 * Fork incoming HTTP requests to handle the concurrently.
 *
 * @param   sfd         Server socket file descriptor.
 * @return  Exit status of server (EXIT_SUCCESS).
 *
 * The parent should accept a request and then fork off and let the child
 * handle the request.
 **/
int forking_server(int sfd) {
    /* Accept and handle HTTP request */
    pid_t id;
    Request *request;

    while (true) {
    	/* Accept request */
        request = accept_request(sfd);
        if(request == NULL){ //if we could not accept, keep trying
            continue;
        }
	/* Ignore children */
        signal(SIGCHLD,SIG_IGN); //ignore my kiddos

	/* Fork off child process to handle request */

        id = fork();

        //NOTE TO SELF: the child should not be allowed to loop, or this will be a fork bomb.
        if(id<0){ //child was not created
            log("UNABLE TO FORK, CHILD NOT CREATED\n");
            free_request(request); //free up that structure
            exit(EXIT_FAILURE); //terminate process with failure code
        } else if(id ==0){ //i am a kiddo, so I should handle requests
            handle_request(request);
            free_request(request);
            close(sfd);
            exit(EXIT_SUCCESS);
        } else { //parent process
            free_request(request);
        }
    }

    /* Close server socket */
    close(sfd);
    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
