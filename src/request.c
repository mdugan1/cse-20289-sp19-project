/* request.c: HTTP Request Functions */

#include "spidey.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <unistd.h>

int parse_request_method(Request *r);
int parse_request_headers(Request *r);

/**
 * Accept request from server socket.
 *
 * @param   sfd         Server socket file descriptor.
 * @return  Newly allocated Request structure.
 *
 * This function does the following:
 *
 *  1. Allocates a request struct initialized to 0.
 *  2. Initializes the headers list in the request struct.
 *  3. Accepts a client connection from the server socket.
 *  4. Looks up the client information and stores it in the request struct.
 *  5. Opens the client socket stream for the request struct.
 *  6. Returns the request struct.
 *
 * The returned request struct must be deallocated using free_request.
 **/
Request * accept_request(int sfd) {
    Request *r;
    struct sockaddr raddr;
    socklen_t rlen = sizeof(struct sockaddr);

    /* Allocate request struct (zeroed) */
    r = calloc(1,sizeof(Request)); //calloc achieves a zeroed request struct
    if(r == NULL){ //if calloc failed, exit with failure status
        printf("ERROR - Failed to allocate space for request struct. \n");
        exit(EXIT_FAILURE);
    }
    /* Accept a client */
    
    //initialize headers list
    r->headers = calloc(1,sizeof(struct header));
    if(r->headers == NULL){
        printf("ERROR - Failed to initialize the list of headers: %s \n",strerror(errno));
        goto fail; //failure is inevitable
    }

    //accept the client
    r->fd = accept(sfd,&raddr,&rlen);    
    if( r->fd == -1){
        printf("ERROR - FAILED TO ACCEPT CLIENT: %s \n", strerror(errno));
        goto fail; //failure is always an option
    }


    /* Lookup client information */
    if((getnameinfo(&raddr,rlen,r->host,sizeof(r->host),r->port,sizeof(r->port),NI_NAMEREQD))){
        printf("ERROR - getnameinfo FAILED: %s \n",strerror(errno));
        goto fail; //failure is part of the learning process
    }

    /* Open socket stream */
    FILE *client_stream = fdopen(r->fd, "r+");
    if(client_stream == NULL){
        printf("ERROR - fdopen FAILED: %s \n",strerror(errno));
        //close(r->fd); //failed to open the stream to and rrom the client
        return NULL; //failed to open stream, fail out of the program
    }
    r->file = client_stream;

    log("Accepted request from %s:%s", r->host, r->port);
    return r;

fail:
    /* Deallocate request struct */
    if(r){
        free_request(r);
    }
    return NULL;
}

/**
 * Deallocate request struct.
 *
 * @param   r           Request structure.
 *
 * This function does the following:
 *
 *  1. Closes the request socket stream or file descriptor.
 *  2. Frees all allocated strings in request struct.
 *  3. Frees all of the headers (including any allocated fields).
 *  4. Frees request struct.
 **/
void free_request(Request *r) {
    if (!r) {
    	return;
    }

    /* Close socket or fd */
    if(r->fd >=0){ //close file descriptor
        close(r->fd);
    }

    if(r->file){ //close file
        fclose(r->file);
    }
    /* Free allocated strings */
    if(r->method)
        free(r->method);
    if(r->path)
        free(r->path);
    if(r->uri)
        free(r->uri);
    if(r->query)
        free(r->query);


    /* Free headers */
    
    //create two headers to iterate through the list
    struct header *h = r->headers;
    struct header *hprev = h;
    while(h){ //while there exist headers still
        
        h = h->next; //get the next header
        
        free(hprev->value); //free the prev header
        free(hprev->name);
        free(hprev);
        
        hprev = h; //set the prev header to curr. header
    }

    /* Free request */
    if(r)
        free(r);
}

/**
 * Parse HTTP Request.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * This function first parses the request method, any query, and then the
 * headers, returning 0 on success, and -1 on error.
 **/
int parse_request(Request *r) {
    /* Parse HTTP Request Method */
    if(parse_request_method(r)){
        printf("ERROR: parse_request_method has failed\n");
        return EXIT_FAILURE;
    }
    /* Parse HTTP Requet Headers*/
    if((parse_request_headers(r))){
        printf("ERROR: parse_request_headers has failed \n");
        return EXIT_FAILURE;
    }


    return 0;
}

/**
 * Parse HTTP Request Method and URI.
 * 
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * HTTP Requests come in the form
 *
 *  <METHOD> <URI>[QUERY] HTTP/<VERSION>
 *
 * Examples:
 *
 *  GET / HTTP/1.1
 *  GET /cgi.script?q=foo HTTP/1.0
 *
 * This function extracts the method, uri, and query (if it exists).
 **/
int parse_request_method(Request *r) {
    char buffer[BUFSIZ];
    char *method;
    char *uri;
    char *query;

    /* Read line from socket */
    if(!fgets(buffer,BUFSIZ,r->file)){
        goto fail;
    }
    /* Parse method and uri */
    method = strtok(skip_whitespace(buffer),WHITESPACE); //read by element, get first element. This removes leading whitespace, as well
    if(method == NULL){
        printf("There is no method. RIP. \n");
        goto fail;
    }
    
    //copy method into r->method
    r->method = calloc(sizeof(char),strlen(method) + 1);
    if(r->method == NULL){
        goto fail;
    }
    strcpy(r->method,method);

    /* Parse query from uri */
    uri = strtok(NULL,WHITESPACE); //get the remainder of the uri
    if(!uri){
        printf("FATAL ERROR: URI is NULL \n");
        free(r->method);
        goto fail;
    }

    uri = strtok(uri,"?"); //get the part before the question mark
    if(uri){
        query = strtok(NULL,WHITESPACE); //use strtok to get second token
    } else{ //we have an issue and need to try to resolve it
        char* yeet = skip_nonwhitespace(buffer);
        if(yeet){
            uri = skip_whitespace(yeet);
            uri = strtok(uri,WHITESPACE);
            query = NULL; //eliminate the query
        } else{
            free(r->method);
            goto fail;
        }
    }

    r->uri = calloc(sizeof(char),strlen(uri)+1);
    if(r->uri == NULL){
        printf("ERROR: No more memory. \n");
    }
    strcpy(r->uri,uri);

    if(query != NULL){ //if query is a valid string
        r->query = calloc(sizeof(char),strlen(query)+1);
        if(r->query == NULL){
            printf("ERROR: NO MEMORY FOR QUERY \n");
            free(r->method);
            free(r->uri);
            goto fail;
        }
        strcpy(r->query,query);
    } else {
        r->query = calloc(sizeof(char),2);
        if(r->query == NULL){
            printf("ERROR: NO MEMORY FOR EMPTY QUERY \n");
            free(r->method);
            free(r->uri);
            goto fail;
        }
        strcpy(r->query,"");
    }


    /* Record method, uri, and query in request struct */
    debug("HTTP METHOD: %s", r->method);
    debug("HTTP URI:    %s", r->uri);
    debug("HTTP QUERY:  %s", r->query);

    return 0;

fail:
    return -1;
}

/**
 * Parse HTTP Request Headers.
 *
 * @param   r           Request structure.
 * @return  -1 on error and 0 on success.
 *
 * HTTP Headers come in the form:
 *
 *  <NAME>: <VALUE>
 *
 * Example:
 *
 *  Host: localhost:8888
 *  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0
 *  Accept: text/html,application/xhtml+xml
 *  Accept-Language: en-US,en;q=0.5
 *  Accept-Encoding: gzip, deflate
 *  Connection: keep-alive
 *
 * This function parses the stream from the request socket using the following
 * pseudo-code:
 *
 *  while (buffer = read_from_socket() and buffer is not empty):
 *      name, value = buffer.split(':')
 *      header      = new Header(name, value)
 *      headers.append(header)
 **/
int parse_request_headers(Request *r) {
    struct header *curr = NULL;
    char buffer[BUFSIZ];
    char *name;
    char *value;

    /* Parse headers from socket */

    while(fgets(buffer,BUFSIZ,r->file) && strlen(buffer) >2){
        chomp(buffer); //fix it
        value = strchr(buffer, ':'); //get first occurrence of ':'
        debug("Value: %s \n",value); //to the debugger
        if(!value){
            return EXIT_FAILURE;
        }
        value = skip_whitespace(++value); //get to the next word
        name = strtok(buffer, ":"); //first token is name
        debug("Name: %s \n", name);
        if(!name){
            return EXIT_FAILURE;
        }

        curr = malloc(sizeof(struct header)); //make curr in heap memory
        if(curr == NULL){
            printf("FAILED TO MALLOC HEADER \n");
            goto fail;
        }
        
        //commit the parts of curr to curr
        
        curr->value = calloc(sizeof(char),strlen(value) + 1);
        if(curr->value == NULL){
            free(curr);
            printf("FAILED TO CALLOC VALUE");
            goto fail;
        }
        strcpy(curr->value,value);

        curr->name = calloc(sizeof(char),strlen(name) + 1);
        if(curr->name == NULL){
            free(curr->value);
            free(curr);
            printf("ERROR: FAILED TO CALLOC CURR->NAME");
            goto fail;
        }

        //set up for next iteration by building headers
        curr->next = r->headers;
        r->headers = curr;
        

    }

#ifndef NDEBUG
    for (struct header *header = r->headers; header; header = header->next) {
    	debug("HTTP HEADER %s = %s", header->name, header->value);
    }
#endif
    return 0;

fail:
    return -1;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
