/* handler.c: HTTP Request Handlers */

#include "spidey.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

/* Internal Declarations */
Status handle_browse_request(Request *request);
Status handle_file_request(Request *request);
Status handle_cgi_request(Request *request);
Status handle_error(Request *request, Status status);

/** uri
 * Handle HTTP Request.
 *
 * @param   r           HTTP Request structure
 * @return  Status of the HTTP request.
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
Status  handle_request(Request *r) {
    Status result;

    /* Parse request */

    struct stat s;
    if(parse_request(r)<0){
        result = handle_error(r,HTTP_STATUS_BAD_REQUEST);
        log("ERROR: FAILED TO PARSE REQUEST\n");
        return result;
    }

    /* Determine request path */
    
    r->path = determine_request_path(r->uri);
    debug("HTTP REQUEST PATH: %s", r->path); //determine the request path for a given uri
    if(r->path == NULL){
        return handle_error(r,HTTP_STATUS_BAD_REQUEST);
    }
    if(!r->path){ //if file not found
        result = handle_error(r,HTTP_STATUS_NOT_FOUND);
        log("Bad Request: Page Note Found \n");
        return result;
    }


    /* Dispatch to appropriate request handler type based on file type */
    if(stat(r->path,&s) == -1){ //errors return -1
        log("ERROR HANDLED ON STAT CALL \n");
        result = handle_error(r,HTTP_STATUS_NOT_FOUND); //assume not found if stat fails.
    } else if ((s.st_mode & S_IFMT) == S_IFDIR){ //check for directory
        result = handle_browse_request(r);
        log("Directory requested \n");
    } else if (S_ISREG(s.st_mode) && !access(r->path,X_OK)){ //CGI File, executable by world
        log("Remote code execution requested\n");
        result = handle_cgi_request(r);
    } else if (S_ISREG(s.st_mode) && !access(r->path,R_OK)){ //regular file, not executable by world
        log("Simple file requested.\n");
        result = handle_file_request(r);
    } else {
        result = handle_error(r,HTTP_STATUS_BAD_REQUEST);
        log("ODD FILE REQUESTED\n");
    }

    log("HTTP REQUEST STATUS: %s", http_status_string(result));

    return result;
}

/**
 * Handle browse request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP browse request.
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error
 * with HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_browse_request(Request *r) {
    struct dirent **entries;
    int n;

    log("Handling BROWSING request");

    /* Open a directory for reading or scanning */
    n = scandir(r->path, &entries, NULL, alphasort); // get a directory scanned and alphabetically sorted, like required for problem statement
    if(n == -1){
        log("ERROR OPENING DIRECTORY \n");
        return handle_error(r,HTTP_STATUS_NOT_FOUND);
    }

    /* Write HTTP Header with OK Status and text/html Content-Type */
    fprintf(r->file,"HTTP/1.0 200 OK\r\nContent-Type: text/html\r\n\r\n"); 

    
    /* For each entry in directory, emit HTML list item */
    fprintf(r->file, "<ul>\n"); //begin with opening list
    for(int i = 0; i<n; i++){
        if(!streq(entries[i]->d_name, ".")){ //omit the current directory dot, but not the previous directory dot
            if(streq("/",r->uri)){ //don't print an additional slash
                fprintf(r->file, "<li><a href=\"/%s\">%s</a></li>\n",entries[i]->d_name,entries[i]->d_name);
            }else{ //do print a more complicated uri
                fprintf(r->file, "<li><a href=\"%s/%s\">%s</a></li>\n",r->uri,entries[i]->d_name,entries[i]->d_name);
            }
        }
    }
    fprintf(r->file,"</ul>\n");


    /* Flush socket, return OK */
    for(int i=0; i<n; i++){ //free each entry individually
        free(entries[i]);
    }
    free(entries); //free the directory array of entries
    fflush(r->file); //flush!
    return HTTP_STATUS_OK;
}

/**
 * Handle file request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_file_request(Request *r) {
    FILE *readMe;
    char buffer[BUFSIZ];
    char *mimetype = NULL;
    size_t nread;

    /* Open file for reading */
   
    if(!r->path){
        return handle_error(r, HTTP_STATUS_BAD_REQUEST);
    }
     
    readMe = fopen(r->path, "r");
    if(!readMe){
        log("Failed to open file for reading \n");
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }
    /* Determine mimetype */
    mimetype = determine_mimetype(r->path); //get type of path

    /* Write HTTP Headers with OK status and determined Content-Type */
    fprintf(r->file, "HTTP/1.0 200 OK\r\nContent-Type: %s\r\n\r\n",mimetype); // load it up

    /* Read from file and write to socket in chunks */
    while((nread = fread(buffer,1,BUFSIZ,readMe))>0){ //so long as this doesn't fail
        if((fwrite(buffer,1,nread,r->file))<0){
            log("Failed to write to buffer, fatal error.\n");
            goto fail;
        }
    }


    /* Close file, flush socket, deallocate mimetype, return OK */
    free(mimetype);
    fclose(readMe);
    fflush(r->file);

    return HTTP_STATUS_OK;
        

fail:
    /* Close file, free mimetype, return INTERNAL_SERVER_ERROR */
    if(readMe){
        fclose(readMe);
        fflush(r->file);
    }

    if(mimetype){ //if not null, free it
        free(mimetype);
    }
    
    return HTTP_STATUS_INTERNAL_SERVER_ERROR;
}

/**
 * Handle CGI request
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
Status  handle_cgi_request(Request *r) {
    FILE *pfs;
    char buffer[BUFSIZ];

    /* Export CGI environment variables from request:
     * http://en.wikipedia.org/wiki/Common_Gateway_Interface */

    log("Handling CGI Requests...");

    if (setenv("DOCUMENT_ROOT",RootPath,1)){
        log("Error: can't set environmental DOCUMENT_ROOT");
    }
    if (setenv("QUERY_STRING",r->query, 1)){
        log("Error: can't set environmental QUERY_STRING");
    }
    if (setenv("REMOTE_ADDR", r->host,1)){
        log("Error: can't set environmental REMOTE_ADDR");
}
    if (setenv("REMOTE_PORT", r->port, 1)){
        log("Error: can't set environmental REMOTE_PORT");
    }
    if (setenv("REQUEST_METHOD", r->method, 1)){
        log("Error: can't set environmental REQUEST_METHOD");
    }
    if (setenv("REQUEST_URI", r->uri ,1)){
        log("Error: can't set environmental REQUEST_URI");
    }
    if (setenv("SCRIPT_FILENAME", r->path, 1)){
        log("Error: can't set environmental SCRIPT_FILENAME");
    }
    if (setenv("SERVER_PORT", Port, 1)){
        log("Error: can't set environmental SERVER_PORT");
    }

    /* Export CGI environment variables from request headers */

    //initialize header pointer for iterator
    struct header *h = r->headers;
    
    //get env vars from headers
    while(h->name){
        if(streq(h->name,"Accept")){
            if(setenv("HTTP_ACCEPT",h->value,1)){
                log("Error: can't set environmental HTTP_ACCEPT");
            }
        }
        if(streq(h->name,"Accept-Encoding")){
            if(setenv("HTTP_ACCEPT_ENCODING",h->value,1)){
                log("Error: can't set environmental HTTP_ACCEPT_ENCODING");
            }
        }
        if(streq(h->name,"Accept-Language")){
            if(setenv("HTTP_ACCEPT_LANGUAGE",h->value,1)){
                log("Error: can't set environmental HTTP_ACCEPT_LANGUAGE");
            }
        }
        if(streq(h->name,"Connection")){
            if(setenv("HTTP_CONNECTION",h->value,1)){
                log("Error: can't set environmental HTTP_CONNECTION");
            }
        }
        if(streq(h->name,"Host")){
            if(setenv("HTTP_HOST",h->value,1)){
                log("Error: can't set environmental HTTP_HOST");
            }
        }
        if(streq(h->name,"User-Agent")){
            if(setenv("HTTP_USER_AGENT",h->value,1)){
                log("Error: can't set environmental HTTP_USER_AGENT");
            }
        }
        h = h->next; //iterate through
    }


    /* POpen CGI Script */
    pfs = popen(r->path,"r");
    if(!pfs){
        pclose(pfs);
        return handle_error(r,HTTP_STATUS_INTERNAL_SERVER_ERROR);
    }
    /* Copy data from popen to socket */
    while(fgets(buffer,BUFSIZ,pfs)){
        fputs(buffer, r->file);
    }

    fflush(r->file);
    pclose(pfs);
    return HTTP_STATUS_OK;


    /* Close popen, flush socket, return OK */
    return HTTP_STATUS_OK;
}

/**
 * Handle displaying error page
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP error request.
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
Status  handle_error(Request *r, Status status) {
    const char *status_string = http_status_string(status);

    /* Write HTTP Header */
    fprintf(r->file, "HTTP/1.0 %s\r\nContent-Type: text/html\r\n\r\n",status_string);
    /* Write HTML Description of Error*/
    if(status == HTTP_STATUS_BAD_REQUEST){
        fprintf(r->file, "<html><body><h1>%s</h1><p>Your request is bad and you should feel bad</p></body></html>",status_string);
    } else if(status == HTTP_STATUS_NOT_FOUND){
        fprintf(r->file, "<html><body><h1>%s</h1><p>Are you lost? You may want some </p><a href=\"https://www.youtube.com/watch?v=2Q_ZzBGPdqE\">help.</a></body></html>",status_string);
        log("this happened.\n");
    } else if(status == HTTP_STATUS_INTERNAL_SERVER_ERROR){
        fprintf(r->file, "<html><body><h1>%s</h1><iframe src =\"https://ndsmcobserver.com\"><p>Time for some news.</p></iframe></body></html>",status_string);
    } else{ //how did I get here
        return status;
    }

    fflush(r->file); //flush it thru
    /* Return specified status */
    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
