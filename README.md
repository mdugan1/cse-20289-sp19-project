# Project - README

This is the [Final Project] for [CSE 20289 Systems Programming (Spring 2019)].

## Members

- Michael Dugan (mdugan1@nd.edu)

## Demonstration

- https://docs.google.com/presentation/d/1tESa-XPqez5A1aWORyoLN9nzCMkhDbDAoWhmQvCToN4/edit?usp=sharing

## Errata

Instead of giving a bad header a 400 response, it gives it a 200 response when it is forked, but directs it to the root page. The env.sh command does indeed show the variables of the host machine. I also added an images folder, which is currently throwing off some of the tests.

## Contributions

Michael Dugan is the only group member.




[Final Project]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp19/project.html
[CSE 20289 Systems Programming (Spring 2019)]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp19/
